# Twitter

Run application

Go to folder

```
$ cd twitter
```

## Npm setup

```
$ npm install
```

## Bower setup

```
$ cd public/vendor
$ bower install
```

## Run server

```
$ npm start
```

## Run Gulp

*production*

```
$ npm run build
```

or

*development*

```
$ gulp
```

## Browser

App running at: [http://localhost:3000](http://localhost:3000)