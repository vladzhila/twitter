
/**
 * Module dependencies
 */

var express       = require('express')
  , path          = require('path')
  , favicon       = require('serve-favicon')
  , logger        = require('morgan')
  , cookieParser  = require('cookie-parser')
  , bodyParser    = require('body-parser')
  , errorHandler  = require('errorhandler')
  , config        = require('config')
  , log           = require('libs/log')(module)
  , HttpError     = require('error').HttpError
  , session       = require('express-session')
  , sessionStore  = require('libs/sessionStore')
  , app           = express();


/**
 * Views engine
 */

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


/**
 * Middleware
 */

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
  secret: config.get('session:secret'),
  key:    config.get('session:key'),
  resave: config.get('session:resave'),
  cookie: config.get('session:cookie'),
  store:  sessionStore,
  saveUninitialized: config.get('session:saveUninitialized')
}));


/**
 * Stylus
 */

app.use(require('stylus').middleware(path.join(__dirname, 'public')));


/**
 * Static
 */

app.use(express.static(path.join(__dirname, 'public')));


/**
 * Middleware
 */

app.use(require('middleware/sendHttpError'));
app.use(require('middleware/loadUser'));


/**
 * Routes
 */

require('routes')(app);


/**
 * Error handlers
 */

app.use(function(err, req, res, next) {

  // e.g. next(404)
  if (typeof err == 'number') {
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    if (app.get('env') == 'development') {
      errorHandler()(err, req, res, next);
    } else {
      log.error(err);
      err = new HttpError(500);
      res.sendHttpError(err);
    }
  }

});

module.exports = app;