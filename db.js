
var mongoose = require('libs/mongoose')
  , async    = require('async');

async.series([
  open,
  dropDb,
  requireModels,
  createUsers
], function(err, results) {
  mongoose.disconnect();
});

function open(cb) {
  mongoose.connection.on('open', cb);
}

function dropDb(cb) {
  var db = mongoose.connection.db;
  db.dropDatabase(cb); // node native driver
}

function requireModels(cb) {
  require('models/user');

  // как только все индексы будут созданы вызовется cb
  async.each(Object.keys(mongoose.models), function(modelName, cb) {
    mongoose.models[modelName].ensureIndexes(cb);
  }, cb);

}

function createUsers(cb) {

  var users = [{
    fullname: 'Vlad Zhila',
    email:    'vlad3955@gmail.com',
    username: 'vladzhila',
    password: 'vlad',
    isAdmin:  true
  }];

  // cb -> null(success) or err(failed)
  async.each(users, function(userData, cb) {
    var user = new mongoose.models.User(userData);
    user.save(cb);
  }, cb);

}