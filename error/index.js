
var util = require('util')
  , http = require('http');

/**
 * Our Http Error
 * @param status
 * @param msg
 * @constructor
 */

function HttpError(status, msg) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, HttpError);

  this.status = status;
  this.message = msg || http.STATUS_CODES[status] || 'Error';
}

util.inherits(HttpError, Error);
HttpError.prototype.name = 'HttpError';
exports.HttpError = HttpError;