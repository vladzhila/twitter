
'use strict';

var gulp          = require('gulp')
  , sourcemaps    = require('gulp-sourcemaps')
  , stylus        = require('gulp-stylus')
  , gulpIf        = require('gulp-if')
  , uglify        = require('gulp-uglify')
  , concat        = require('gulp-concat')
  , rename        = require('gulp-rename')
  , autoprefixer  = require('autoprefixer-stylus')
  , del           = require('del')
  , isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';


/**
 * Styles
 */

gulp.task('styles', function() {

  return gulp.src('public/stylus/style.styl')
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(stylus({
      compress: true,
      use: [autoprefixer('last 2 version')]
    }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('public/bundle/styles'));
});


/**
 * Js
 */

gulp.task('js', function() {

  return gulp.src('public/js/**/*.*js', { base: 'public' })
    .pipe(concat('bundle.min.js'))
    .pipe(gulpIf(!isDevelopment, uglify()))
    .pipe(gulp.dest('public/bundle/js'));
});


/**
 * Js vendor
 */

gulp.task('vendor', function() {

    return gulp.src([
      'public/vendor/bower_components/angular/angular.min.js',
      'public/vendor/bower_components/angular-resource/angular-resource.min.js',
      'public/vendor/bower_components/angular-route/angular-route.min.js',
      'public/vendor/bower_components/angular-animate/angular-animate.min.js',
      'public/vendor/bower_components/angular-sanitize/angular-sanitize.min.js',
      'public/vendor/bower_components/ng-file-upload/ng-file-upload.min.js'
    ])
    .pipe(concat('vendor.bundle.min.js'))
    .pipe(gulp.dest('public/bundle/js'));
});


/**
 * Delete bundle folder
 */

gulp.task('clean', function() {
  return del('public/bundle');
});


/**
 * Build
 */

gulp.task('build', gulp.series('clean', 'styles', 'js', 'vendor'));


/**
 * Watch
 */

gulp.task('watch', function() {
  gulp.watch('public/stylus/**/*.*', gulp.series('styles'));
  gulp.watch('public/js/**/*.*', gulp.series('js'));
});


/**
 * Run gulp
 */

gulp.task('default', gulp.series('build', 'watch'));