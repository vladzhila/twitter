
var mongoose = require('mongoose')
  , session = require('express-session')
  , MongoStore = require('connect-mongo')(session);

var sessionStore = new MongoStore({ mongooseConnection: mongoose.connection });

module.exports = sessionStore;