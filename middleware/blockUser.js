
module.exports = function(req, user, next) {

  var io = req.app.get('io')
    , connectedSockets = io.of('/').connected
    , sessionStore = require('libs/sessionStore');

  // for each connected user
  Object.keys(connectedSockets).forEach(function(socketId) {
    var socket = connectedSockets[socketId];

    // destroy user session
    if (socket.handshake.user.username == user.username) {
      sessionStore.destroy(socket.handshake.session.id, function(err) {
        socket.emit('logout');
        socket.disconnect();
        if (err) return next(err);
      });
    }
  });

};