
var User   = require('models/user').User
  , Tweets = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments
  , async = require('async')
  , fs   = require('fs');

// delete profile
// POST /delete_profile/:id

module.exports = function(req, res, next) {

  // id user that we want to delete
  var userId = req.params.id;

  async.waterfall([
    findTweets,
    deleteAttached,
    deleteTweets,
  ], function(err, results) {
    if (err) return next(err);
  });

  function findTweets(cb) {
    Tweets.find({ user: userId }, cb);
  }

  function deleteAttached(tweets, cb) {
    tweets.forEach(function(tweet) {
      if (tweet.image !== '') {
        console.log( 'ok' );
        fs.unlinkSync('public/img/attached/' + tweet.image);
      }
    });
    cb();
  }

  function deleteTweets(cb) {
    Tweets.remove({ user: userId }, cb);
  }

  // delete user comments
  Comments.find({ user: userId }).remove(function(err) {
    if (err) return next(err);  
  });

  User.findById(userId, function(err, user) {
    if (err) return next(err);

    // delete user avatar
    if (user.avatar != 'avatar.png') {
      fs.unlinkSync('public/img/' + user.avatar);
    }

    // delete our user in follows other users
    user.followers.forEach(function(follower) {
      deleteUserId(follower, { follows: userId });
    });

    // delete our user in followers other users
    // and queries friends
    user.follows.forEach(function(following) {
      deleteUserId(following, {
        followers: userId,
        queryFriends: userId
      });
    });

    // delete our user in friends other users
    user.friends.forEach(function(friend) {
      deleteUserId(friend, { friends: userId });
    });

    /**
     * Delete id user in fields other users
     * @param  {[String]} id     [ObjectId user that we want to delete]
     * @param  {[Object]} fields [fields other users where we want ot delete our user id]
     */
     
    function deleteUserId(id, fields) {
      User.findOneAndUpdate({ _id: id }, {
        $pull: fields
      }, function(err){
        if (err) return next(err);
      });
    }

  });

  next();

};