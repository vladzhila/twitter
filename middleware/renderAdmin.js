
var User = require('models/user').User
  , HttpError = require('error').HttpError;

module.exports = function(req, res, next) {
  User.findById(req.session.user, function(err, user) {
    if (err) return next(err);

    if (user.isAdmin) {
      next();
    } else {
      return next(new HttpError(403, 'Нету доступа!'));
    }
    
  });
};