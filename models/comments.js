
var mongoose = require('libs/mongoose')
  , Schema = mongoose.Schema;

var commentsSchema = new Schema({
  content: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  tweet: {
    type: Schema.Types.ObjectId,
    ref: 'Tweets',
    required: true
  },
  likes : [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }]
});

exports.Comments = mongoose.model('Comments', commentsSchema);