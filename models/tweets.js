
var mongoose = require('libs/mongoose')
  , Schema = mongoose.Schema;

var tweetsSchema = new Schema({
  content: {
    type: String,
    required: true
  },
  image: {
    type: String,
    default: ''
  },
  created: {
    type: Date,
    default: Date.now
  },
  private: {
    type: Boolean,
    default: false
  },
  retweets : [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  comments : [{
    type: Schema.Types.ObjectId,
    ref: 'Comments'
  }],
  user : {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  likes : [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }]
});

exports.Tweets = mongoose.model('Tweets', tweetsSchema);