
var crypto    = require('crypto')
  , mongoose  = require('libs/mongoose')
  , async     = require('async')
  , fs        = require('fs')
  , AuthError = require('error/authError').AuthError

Schema = mongoose.Schema;

var schema = new Schema({
  fullname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  username: {
    type: String,
    unique: true,
    required: true
  },
  avatar: {
    type: String,
    default: 'avatar.png'
  },
  retweets:[{
    type: Schema.Types.ObjectId,
    ref: 'Tweets'
  }],
  follows:[{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  followers:[{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  friends:[{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  queryFriends:[{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  isAdmin: {
    type: Boolean,
    default: false
  },
  block: {
    type: Boolean,
    default: false
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  }
});

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
  .set(function(password) {
    this._plainPassword = password;
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() { return this._plainPassword; });

schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};


/**
 * Login
 */

schema.statics.authorize = function(username, password, callback) {
  var User = this;

  async.waterfall([
    function(callback) {
      User.findOne({ username: username }, callback);
    },
    function(user, callback) {
      if (user) {
        if (user.checkPassword(password)) {
          callback(null, user);
        } else {
          callback(new AuthError("Пароль неверен"));
        }
      } else {
        callback(new AuthError("Пароль неверен"));
      }
    }
  ], callback);
};


/**
 * Sign up
 */

schema.statics.signup = function(fullname, email, username, password, callback) {
  var User = this;

  var user = new User({
    fullname: fullname,
    email:    email,
    username: username,
    password: password
  });

  user.save(function(err) {
    if (err) return callback(err);
    callback(null, user);
  });
};

exports.User = mongoose.model('User', schema);