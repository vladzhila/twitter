(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('anotherUserCtrl', ['$scope', '$resource', 'logoutService',
			function($scope, $resource, logoutService) {

				// status button unfollow 'Читаю' или 'Отмена', default: 'Читаю'
				$scope.read = 'Читаю';

				/**
				 * Subscribe to user
				 * @param  {[String]} id [ObjectId of user on which subscribe]
				 */
				$scope.follow = function(id) {
					$resource('follow/' + id).save(function() {
						location.reload();
					});
				};

				/**
				 * Unsubscribe from user
				 * @param  {[String]} id [ObjectId of user from which unsubscribe]
				 */
				$scope.unfollow = function(id) {
					$resource('unfollow/' + id).save(function() {
						location.reload();
					});
				};

				$scope.logout = function() {
					logoutService.logout();	
				};

			}
		]);

})();

