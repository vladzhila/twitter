(function() {
	'use strict';

	/* App Module */

	angular.module('config', []).
		config(['$routeProvider', '$locationProvider',
			function($routeProvider, $locationProvider) {

			// remove /# in url
			$locationProvider.html5Mode(true);

			$routeProvider.
				when('/', {
					templateUrl: 'partials/home',
					controller: 'homeCtrl'
				}).
				when('/edit_profile', {
					templateUrl: 'partials/edit-profile',
					controller: 'editCtrl'
				}).
				when('/friends', {
					templateUrl: 'partials/friends',
					controller: 'friendCtrl'
				}).
				when('/friends_queries', {
					templateUrl: 'partials/friends',
					controller: 'friendCtrl'
				}).
				when('/admin', {
					templateUrl: 'partials/admin/home',
					controller: 'adminCtrl'
				}).
				when('/admin/users', {
					templateUrl: 'partials/admin/users',
					controller: 'adminCtrl'
				}).
				when('/admin/tweets', {
					templateUrl: 'partials/admin/tweets',
					controller: 'adminCtrl'
				}).
				when('/:username', {
					templateUrl: function(url) {
						return 'check-user/' + url.username;
					}
				}).
				when('/current-user', {
					templateUrl: 'current-user'
				}).
				when('/another-user', {
					templateUrl: 'another-user'
				}).
				otherwise({
					redirectTo: '/'
				});
		}]);
	
})();