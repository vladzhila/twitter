(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('controller', ['$scope', '$resource', '$http',
			function($scope, $resource, $http) {

				$scope.newTweet = '';
				$scope.formData = {};

				// ADD TWEET ==========================

				$scope.addTweet = function() {

					var lengthTweet = $scope.newTweet.length;

					// 1-140 symbols
					if (lengthTweet && lengthTweet <= 140) {

						$scope.formData.tweet = $scope.newTweet;

						$resource(location.pathname).save($scope.formData, function() {
							location.reload();
							$scope.formData = {}; // reset data form
						});

					}

				};

				// DELETE TWEET ==========================

				$scope.deleteTweet = function(tweet) {

					if (!confirm('Вы действительно ходите удалить твит?')) return;

					$http.post('/delete/' + tweet).then(function() {
						location.reload();
					});

					// $resource('/delete/' + tweet).remove(function() {
					// 	location.reload();
					// });
				};

				// LOGOUT ==========================
				
				$scope.logout = function() {
					$resource('/logout').save(function() {
						location.href = '/';
					});
				};

			}
		]);

})();

