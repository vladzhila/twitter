(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('adminCtrl', [
      '$scope', 'Admin', 'Common',
      function($scope, Admin, Common) {

        Admin.home();
        Admin.users();
        Admin.tweets();

        // TWEET like, delete
        // COMMENTS show, hide, add, delete, like
        // SEARCH
        // LOGOUT from site
        Common.get();

        $scope.showAdmins = function(user) {
          return $scope.onlyAdmins ? user.isAdmin : user;
        };

        $scope.makeAdmin = function(userId) {
          Admin.makeAdmin(userId);
        };

        $scope.deleteProfile = function(userId) {
          Admin.deleteProfile(userId);
        };

        $scope.toggleBlockUser = function(userId) {
          Admin.blockUser(userId);
        };

      }
    ]);
})();