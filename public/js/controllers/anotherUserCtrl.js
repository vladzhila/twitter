(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('anotherUserCtrl', [
      '$scope', 'User', 'Follow', 'Button', 'Tweets', 'Common',
      function($scope, User, Follow, Button, Tweets, Common) {

        // get user, session user id
        User.get();

        // get tweets
        Tweets.get();

        // get buttons that need to show in page
        Button.get();

        // TWEET like, delete
        // COMMENTS show, hide, add, delete, like
        // SEARCH
        // LOGOUT from site
        Common.get();


        $scope.anotherUser = true;

        // status button unfollow 'Читаю' или 'Отмена', default: 'Читаю'
        $scope.read = 'Читаю';

        // status button remove-friend 'Друг' или 'Отмена', default: 'Друг'
        $scope.friend = 'Друг';


        /**
         * Subscribe to user
         * @param  {[String]} id [ObjectId of user on which subscribe]
         */

        $scope.follow = function(id) {
          Follow.add('follow/', id)
        };

        /**
         * Unsubscribe from user
         * @param  {[String]} id [ObjectId of user from which unsubscribe]
         */

        $scope.unfollow = function(id) {
          Follow.remove('unfollow/', id)
        };

        /**
         * Send query to friends
         * @param  {[String]} id [ObjectId of user which send query to friends]
         */

        $scope.addFriend = function(id) {
          Follow.add('friend/add/', id)
        };

        /**
         * Cancel query to friends
         * @param  {[String]} id [ObjectId of user which cancel query to friends]
         */

        $scope.cancelQueryFriend = function(id) {
          Follow.remove('friend/cancel/', id);
        };

        /**
         * Remove user from friends
         * @param  {[String]} id [ObjectId of user which remove from friends]
         */

        $scope.removeFriend = function(id) {
          Follow.remove('friend/remove/', id);
        };

        /**
         * Apply query to friends
         * @param  {[String]} id [Username of user which apply to friends]
         */

        $scope.applyQuery = function(username) {
          Follow.add('friend/query/apply/', username);
        };

        /**
         * Cancel query to friends
         * @param  {[String]} id [Username of user which cancel to friends]
         */

        $scope.cancelQuery = function(username) {
          Follow.remove('friend/query/cancel/', username);
        };

      }
    ]);
})();