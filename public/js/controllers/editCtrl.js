(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('editCtrl', [
      '$scope', 'Auth', 'User', 'Edit', '$resource',
      function($scope, Auth, User, Edit, $resource) {

        // get user data
        Edit.get();

        /**
         * Change fullname, username, email
         */

        $scope.editMode = function(oldFullname, oldUsername, oldEmail) {

          // display user data in inputs form when click edit button
          $scope.editedFullname = oldFullname;
          $scope.editedUsername = oldUsername;
          $scope.editedEmail = oldEmail;

          // toggle for show and hide user info
          $scope.toggle = true;
        };

        /**
         * Save user fullname, username, email
         * @param  {[String]} id [ObjectID user]
         */

        $scope.editSave = function(id) {
          var fullname = $scope.editedFullname
            , username = $scope.editedUsername
            , email    = $scope.editedEmail;

          if (fullname && email && username) {

            $resource('/edit_info/' + id).save({
              fullname: fullname,
              username: username,
              email: email
            }, function(res) {
              $scope.toggle = false;
              Edit.get();
            }, function(res) {
              if (res.status == 400) return Edit.errorMsg(res.data.error);
              return Edit.errorMsg('Пользователь с данным логином или email уже есть!');
            });

          } else {
            return Edit.errorMsg('Заполните все поля!');
          }
        };

        $scope.cancelSave = function() {
          $scope.toggle = false;
        };

        /**
         * Change avatar
         */

        $scope.toggleSubmitAvatar = true;

        $scope.showChangeAvaBtn = function() {
          $scope.toggleSubmitAvatar = false;
        };

        $scope.changeAvatar = function(id) {
          var fd = new FormData();
          fd.append('file', $scope.avatar);

          Edit.changeAvatar('/avatar/' + id, fd);
          $scope.toggleSubmitAvatar = true;
        };

        $scope.deleteAvatar = function(id) {
          Edit.deleteAvatar(id);
          $scope.toggleSubmitAvatar = true;
        };


        /**
         * Change password
         */

        $scope.editPassword = function() {
          $scope.togglePassword = true;
        };

        $scope.changePassword = function(id) {
          var newPassword = $scope.newPassword,
              oldPassword = $scope.oldPassword;

          if (oldPassword && newPassword) {

            if (oldPassword == newPassword) {
              return Edit.errorMsg('Вы ввели одинаковые пароли!');
            }

            Edit.password(id, {
              oldPassword: oldPassword,
              newPassword: newPassword
            });

            $scope.cancelChange();

          } else {
            return Edit.errorMsg('Заполните все поля!');
          }
        };

        $scope.cancelChange = function() {
          $scope.togglePassword = false;
          $scope.newPassword = $scope.oldPassword = '';
        };

        /**
         * Delete profile
         */

        $scope.deleteProfile = function(userId) {
          User.delete(userId);
        };

        /**
         * Logout
         */

        $scope.logout = function() {
          Auth.logout();
        };

      }
    ]);
})();