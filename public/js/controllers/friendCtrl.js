(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('friendCtrl', [
      '$scope', 'Auth', 'Friends',
      function($scope, Auth, Friends) {

        (location.pathname == '/friends') ?
          // get friends or queries to friends and buttons for apply and cancel query
          Friends.get() : Friends.queries();

        /**
         * Apply query to friends
         * @param  {[String]} username [Username of user which apply to friends]
         */

        $scope.applyQuery = function(username) {
          Friends.apply(username);
        };

        /**
         * Cancel query to friends
         * @param  {[String]} username [Username of user which cancel to friends]
         */

        $scope.cancelQuery = function(username) {
          Friends.cancel(username);
        };


        $scope.logout = function() {
          Auth.logout();
        };

      }
    ]);
})();

