(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('homeCtrl', [
      'Home', 'Common',
      function(Home, Common) {
        // get user, tweets, session user id
        Home.get();
        
        // TWEET like, delete
        // COMMENTS show, hide, add, delete, like
        // SEARCH
        // LOGOUT from site
        Common.get();
      }
    ]);
})();