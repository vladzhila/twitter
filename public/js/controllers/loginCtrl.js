(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('loginCtrl', [
      '$scope', 'Auth',
      function($scope, Auth) {


        /**
         * Login
         */
        
        $scope.login = function() {
          var username = $scope.user.username
            , password = $scope.user.password;

          // when all field are filled
          if (username && password) {
            Auth.login({
              username: username,
              password: password
            });
          } else {
            errorMsgLogin('Заполните все поля!');
          }
        };


        /**
         * Sign up
         */
        
        $scope.signup = function() {
          var fullname = $scope.fullname
            , email    = $scope.email
            , username = $scope.username
            , password = $scope.password;

          if (fullname && email && username && password) {
            Auth.signup({
              fullname: fullname,
              email:    email,
              username: username,
              password: password
            });
          } else {
            return errorMsgSignup('Заполните все поля!');
          }
        };


        /**
         * Error messege
         */

        function errorMsgLogin(text) {
          Auth.errorMsgLogin(text);
        }

        function errorMsgSignup(text) {
          Auth.errorMsgSignup(text);
        }

      }
    ]);
})();