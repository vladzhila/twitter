(function() {
  'use strict';

  angular.module('twitterApp')
    .controller('userCtrl', [
      '$scope', 'User', 'Tweets', 'Attach', 'Common', 'Url',
      function($scope, User, Tweets, Attach, Common, Url) {

        // get user, session user id
        User.get();

        // get tweets
        Tweets.get();
        
        // TWEET like, delete
        // COMMENTS show, hide, add, delete, like
        // SEARCH
        // LOGOUT from site
        Common.get();

        // empty sting by default tweet value
        $scope.newTweet = '';

        /**
         * Add new tweet
         * @param {[String]} userId [ObjectID of user]
         */

        $scope.addTweet = function(userId) {
          var attachedImg, tweetWithLinks, tweet,
              lengthTweet = $scope.newTweet.length,
              checkLenghtTweet = lengthTweet && lengthTweet <= 140;

          // interval 1-140 symbols
          if (!checkLenghtTweet) return;

          attachedImg = $scope.attachedImg;

          // find links in tweet
          tweetWithLinks = Url.find($scope.newTweet);

          tweet = {
            content: tweetWithLinks || $scope.newTweet,
            private: $scope.forFriends
          };

          if (attachedImg) {
            // add tweet with attached image and text
            Attach.image(userId, tweet, attachedImg);
            $scope.clearAttached();
            resetTweetArea();
          } else {
            // add tweet only with text
            Tweets.add(userId, tweet);
            resetTweetArea();
            Common.resetNotifications();
          }

          function resetTweetArea() {
            $scope.newTweet = '';
          }
        };

        /**
         * Remove attached image
         */

        $scope.clearAttached = function() {
          $scope.attachedImg = undefined;
        };

      }
    ]);
})();