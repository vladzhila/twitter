(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('usersCtrl', ['$scope', '$resource', '$http', 'logoutService',
			function($scope, $resource, $http, logoutService) {

				$scope.logout = function() {
					logoutService.logout();	
				};

			}
		]);

})();