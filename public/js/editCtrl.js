(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('editCtrl', [
			'$scope',
			'$resource',
			'$http',
			'$timeout',
			'logoutService',
			'validationService',
			function($scope, $resource, $http, $timeout, logoutService, validationService) {

				// CHANGE FULLNAME, USERNAME, EMAIL =====================================

				$scope.editMode = function(oldFullname, oldUsername, oldEmail) {
					$scope.editedFullname = oldFullname;
					$scope.editedUsername = oldUsername;
					$scope.editedEmail = oldEmail;
					// toggle for show and hide
					$scope.toggle = true;
				};

				$scope.editSave = function(id) {
					var fullname = $scope.editedFullname
						, email    = $scope.editedEmail
						, username = $scope.editedUsername;

					if (fullname && email && username) {
						var msg = validationService.validator(fullname, email, username);

						if (msg != 200) return showErrorMsg(msg);

						$resource('/edit_info/' + id).save({
							'fullname': fullname,
							'username': username,
							'email': email
						}, function() {
							location.reload();
						}, function() {
							return showErrorMsg('Пользователь с данным логином или email уже есть!');
						});
					} else {
						return showErrorMsg('Заполните все поля!');
					}
				};

				$scope.cancelSave = function() {
					$scope.toggle = false;
				};

				// CHANGE AVATAR =====================================

				$scope.toggleSubmitAvatar = true;

				$scope.showChangeAvaBtn = function() {
					$scope.toggleSubmitAvatar = false;
				};

				$scope.deleteAvatar = function(id) {
					$resource('/delete_avatar/' + id).save(function() {
						location.reload();
					});
				};

				// CHANGE PASSWORD =====================================

				$scope.editPassword = function() {
					$scope.togglePassword = true;
				};

				$scope.changePassword = function(id) {

					var newPassword = $scope.newPassword;
					var oldPassword = $scope.oldPassword;

					if (oldPassword && newPassword) {

						if (oldPassword == newPassword) {
							return showErrorMsg('Вы ввели одинаковые пароли!'); 
						}

						$resource('/password/' + id).save({
							'oldPassword': oldPassword,
							'newPassword': newPassword
						}, function() {
							$scope.newPassword = $scope.oldPassword = '';
							$timeout(function() {
								alert('Пароль успешно изменен!');
								location.reload();
							}, 0);
						}, function() {
							return showErrorMsg('Текущий пароль не верный!');
						});
					} else {
						return showErrorMsg('Заполните все поля!');
					}
				};

				$scope.cancelChange = function() {
					$scope.togglePassword = false;
					$scope.newPassword = $scope.oldPassword = '';
				};

				// DELETE PROFILE =====================================

				$scope.deleteProfile = function(userId) {
					if (!confirm('Вы действительно ходите удалить профиль?')) return;
					$http.post('/delete_profile/' + userId).then(function() {
						location.href = '/';
					});
				};

				// LOGOUT =============================================

				$scope.logout = function() {
					logoutService.logout();
				};

				// ERROR MESSAGE ======================================

				function showErrorMsg(text) {
					$scope.errorMessage = text;
					$timeout(function() {
						$scope.errorMessage = '';
					}, 3000);
				}

			}
		]);

})();