(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('homeCtrl', ['$scope', 'logoutService',
			function($scope, logoutService) {

				$scope.logout = function() {
					logoutService.logout();	
				};

			}
		]);

})();

