(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('loginCtrl', [
			'$scope',
			'$resource',
			'$timeout',
			'validationService',
			function($scope, $resource, $timeout, validationService) {

				// SIGN IN ===============================

				$scope.user = { username: '', password: '' };

				$scope.login = function() {

					var username = $scope.user.username
						, password = $scope.user.password;

					// when all field are filled
					if (username && password) {
						// REST post login
						$resource('/login').save({
							'username': username,
							'password': password
						}, function() {
							location.href = '/';
						}, function() {
							showErrorMsg('Пользователь не зарегистрирован или неверный пароль!');
						});
					} else {
						showErrorMsg('Заполните все поля!');
					}
				};

				// SIGN UP ===============================

				$scope.signup = function() {

					var fullname = $scope.fullname
						, email    = $scope.email
						, username = $scope.username
						, password = $scope.password;

					if (fullname && email && username && password) {

						var msg = validationService.validator(fullname, email, username);

						if (msg != 200) return showErrorMsg(msg);

						// REST post sign up
						$resource('/signup').save({
							'fullname': fullname,
							'email' : email,
							'username': username,
							'password': password
						}, function() {
							location.href = '/';
						}, function() {
							return showErrorMsg('Пользователь с данным логином или email уже есть!');
						});
					} else {
						return showErrorMsg('Заполните все поля!');
					}
				};

				function showErrorMsg(text) {
					$scope.errorMessage = text;
					$timeout(function() {
						$scope.errorMessage = '';
					}, 3000);
				}

			}
	]);

})();