(function() {
	'use strict';

	angular.module('twitterApp', [
		'ngResource',
		'ngRoute',
		'ngFileUpload',
		'config',
		'animation',
		'controllers'
	]);

})();