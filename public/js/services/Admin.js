(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Admin', [
      '$rootScope', '$resource',
      function($rootScope, $resource) {

        function getUsers() {
          $resource('/admin/users_page').get(function(res) {
            $rootScope.users = res.users;
          });
        }

        function getTweets() {
          $resource('/admin/tweets_page').get(function(res) {
            $rootScope.tweets = res.tweets;
            $rootScope.sessionUser = res.sessionUser;
          });
        }

        return {
          home: function() {
            $resource('/admin_page').get(function(res) {
              $rootScope.users = res.users;
              $rootScope.tweets = res.tweets;
            });
          },
          users: function() {
            getUsers();
          },
          tweets: function() {
            getTweets();
          },
          makeAdmin: function(id) {
            $resource('/admin/make_admin/' + id).save(function(res) {
              getUsers();
            });
          },
          blockUser: function(id) {
            $resource('/admin/block/' + id).save(function(res) {
              getUsers();
            });
          },
          deleteProfile: function(id) {
            if (!confirm('Вы действительно ходите удалить профиль?')) return;
            $resource('/delete_user/' + id).delete(function(res) {
              getUsers();
            });
          }
        };

      }]);

})();