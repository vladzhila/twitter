(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Attach', [
      '$rootScope', '$resource', '$http', 'socket',
      function($rootScope, $resource, $http, socket) {

        return {
          image: function(userId, tweet, attachedImg) {
            var formData = new FormData();
            formData.append('file', attachedImg);

            $resource('tweet/create/' + userId).save(tweet, function(res) {
              $http.post('attach/image/' + res.lastTweet._id, formData, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
              })
              .success(function(res){
                $rootScope.tweets.unshift(res.tweet);
                socket.emit('notification:tweets', res.tweet);
              });
            });
          }
        };

      }]);

})();