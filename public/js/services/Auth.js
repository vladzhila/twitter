(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Auth', [
      '$rootScope', '$resource', '$timeout',
      function($rootScope, $resource, $timeout) {

        return {
          login: function(data) {
            $resource('/login').save(data, function() {
              location.href = '/';
            }, function(res) {
              if (res.status == 401) {
                return errorMsgLogin(res.data);
              }
              return errorMsgLogin('Пользователь не зарегистрирован или неверный пароль!');
            });
          },
          signup: function(data) {
            $resource('/signup').save(data, function() {
              location.href = '/';
            }, function(res) {
              if (res.status == 400) {
                return errorMsgSignup(res.data.error);
              }
              return errorMsgSignup('Пользователь с данным логином или email уже есть!');
            });
          },
          logout: function() {
            $resource('/logout').save(function() {
              location.href = '/';
            });
          },
          errorMsgLogin: errorMsgLogin,
          errorMsgSignup: errorMsgSignup
        };


        /**
         * Error massage for login and sign up
         */

        function errorMsgLogin(text) {
          $rootScope.errMsgL = text;
          $timeout(function() {
            $rootScope.errMsgL = '';
          }, 3000);
        }
        
        function errorMsgSignup(text) {
          $rootScope.errMsgS = text;
          $timeout(function() {
            $rootScope.errMsgS = '';
          }, 3000);
        }

      }]);

})();