(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Button', [
      '$rootScope', '$resource',
      function($rootScope, $resource) {

        return {
          get: function(url, id) {
            $resource('/buttons' + location.pathname).get(function(res) {
              $rootScope.buttons = res;
            });
          }
        };

      }]);

})();