(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Comments', [
      '$resource', 'Tweets', 'Home', 'Admin', 'socket',
      function($resource, Tweets, Home, Admin, socket) {

        function getTweets() {
          switch (location.pathname) {

            case '/':
              Home.get(); break;

            case '/admin/tweets':
              Admin.tweets(); break;

            default:
              Tweets.get(); break;
              
          }
        }

        return {
          add: function(senderUserId, tweetId, data) {

            var dataComment = {
              senderUserId: senderUserId,
              tweetId: tweetId,
              comment: data.comment
            };

            socket.emit('add:comment', dataComment, function(data) {
              getTweets();
              socket.emit('notification:comments', data);
            });
          },
          delete: function(commentId, senderUserId) {
            if (!confirm('Удалить комментарий?')) return;

            var dataEmit = {
              commentId: commentId,
              senderUserId: senderUserId
            };

            socket.emit('remove:comment', dataEmit, function(data) {
              getTweets();
              socket.emit('notification:comments', data);
            });
          },
          like: function(id) {
            $resource('/comment/like/' + id).save(function() {
              getTweets();
            });
          }
        };

      }]);

})();