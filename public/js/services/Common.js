(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Common', [
      '$rootScope', 'Auth', 'Comments', 'Tweets', 'Home', 'Search', 'socket',
      function($rootScope, Auth, Comments, Tweets, Home, Search, socket) {

        function resetNotifications() {
          $rootScope.newTweets = [];
          $rootScope.newComments = {};
        }

        return {
          resetNotifications: function() {
            resetNotifications();
          },
          get: function() {


            /**
             * Tweets
             */


            /**
             * Like tweet
             * @param {[String]} tweetId [ObjectID of tweet that like]
             */

            $rootScope.like = function(tweetId) {
              resetNotifications()
              Tweets.like(tweetId);
            };


            /**
             * Delete tweet
             * @param {[String]} userId [ObjectID of user]
             * @param {[String]} tweetId [ObjectID of tweet that we want to delete]
             */

            $rootScope.deleteTweet = function(userId, tweetId) {
              resetNotifications()
              Tweets.delete(userId, tweetId)
            };


            /**
             * Comments
             */

            $rootScope.toggleComments = [];
            $rootScope.notHome   = location.pathname != "/";
            $rootScope.pathHome  = location.pathname == '/';
            $rootScope.pathAdmin = location.pathname == '/admin/tweets';

            // show comments

            $rootScope.showComments = function(idx) {
              var activeIdx = $rootScope.activeIdx;

              if (activeIdx != idx) {
                $rootScope.toggleComments[activeIdx] = false;
              }

              $rootScope.toggleComments[idx] = true;
              $rootScope.activeIdx = idx;
            };

            $rootScope.isShowing = function(index){
              return $rootScope.activeIdx === index;
            };

            // hide comments

            $rootScope.hideComments = function(idx) {
              $rootScope.toggleComments[idx] = false;
            };

            // add comment

            $rootScope.addComment = function(senderUserId, tweetId, comment) {
              var lengthComment = comment.length;

              if (lengthComment && lengthComment <= 140) {
                resetNotifications()
                Comments.add(senderUserId, tweetId, { comment: comment });
              }
            };

            // delete comment

            $rootScope.deleteComment = function(commentId, senderUserId) {
              resetNotifications()
              Comments.delete(commentId, senderUserId);
            };

            // like comment

            $rootScope.likeComment = function(commentId) {
              resetNotifications()
              Comments.like(commentId);
            };


            /**
             * Search
             */

            $rootScope.search = function(keyword) {
              Search.getUsers(keyword);
            };


            /**
             * Retweet
             */

            // add retweet

            $rootScope.retweet = function(userId, tweetId) {
              resetNotifications()
              Tweets.retweet(userId, tweetId);
            };

            // delete retweet

            $rootScope.deleteRetweet = function(userId, tweetId) {
              resetNotifications()
              Tweets.deleteRetweet(userId, tweetId);
            };


            /**
             * Auth
             */

            $rootScope.logout = function() {
              Auth.logout();
            };


            /**
             * Notification new tweets on front page 
             */

            // notifications new tweets
            $rootScope.newTweets = [];

            /**
             * Add tweet to notification on front page your followers
             * @param  {[Object]} data [Data with tweet which need add to notification]
             */

            socket.on('add:tweet:notification', function(data) {
              if (location.pathname != '/') return;

              // if tweet already have in notification new tweets then skip
              if (~$rootScope.newTweets.indexOf(data.tweet)) return;

              $rootScope.newTweets.push(data.tweet);
            });

            /**
             * Remove tweet from block new tweets on front page your followers 
             * @param  {[Object]} data [Data with tweetId which need remove from notification]
             */

            socket.on('remove:tweet:notification', function(data) {
              $rootScope.newTweets.forEach(function(tweet, idx, newTweets) {
                if (tweet._id == data.tweetId) {
                  newTweets.splice(idx, 1);
                }
              });
            });

            /**
             * Show new tweets on front pages
             */
            
            $rootScope.showNotificationTweets = function() {
              Home.get();
              $rootScope.newTweets = []; // reset notification
            };


            /**
             * Notification new comments on other pages
             */

            // notifications new comments
            $rootScope.newComments = {};

            /**
             * Add comment to notifications on other pages
             * @param  {[Object]} data [Data with tweet id, comment, id sender]
             */

            socket.on('add:comment:notification', function(data) {
              var tweetId = data.tweetId;

              if ($rootScope.newComments[tweetId] == undefined) {
                $rootScope.newComments[tweetId] = [];
                addNewComment(data)
              } else {
                addNewComment(data)
              }

              // add new comment to notification block
              function addNewComment(dataLastComment) {
                var notificationComments = $rootScope.newComments[tweetId],
                    ntfLength = notificationComments.length,
                    i;

                for (i = 0; i < ntfLength; i++) {
                  if (notificationComments[i].comment._id == dataLastComment.comment._id) {
                    return;
                  }
                }

                $rootScope.newComments[tweetId].push(dataLastComment);
              }
            });

            /**
             * Remove comment from notifications on other pages
             * @param  {[Object]} data [Data with deleted comment which need remove from notifications]
             */

            socket.on('remove:comment:notification', function(data) {
              var deletedComment = data.deletedComment;

              $rootScope.newComments[deletedComment.tweet].forEach(removeComment);

              function removeComment(dataNewComments, idx, newComments) {
                if (dataNewComments.comment._id == deletedComment._id) {
                  newComments.splice(idx, 1);
                }
              }

            });

            /**
             * Show new comments on other pages
             * @param  {[String]} tweetId [ObjectId tweet to which add new comments from the notification unit]
             */

            $rootScope.showNotificationComments = function(tweetId) {
              $rootScope.newComments[tweetId].forEach(function(newComment) {
                $rootScope.tweets.forEach(function(tweet, idx, tweets) {
                  if (tweet._id == newComment.tweetId) {
                    tweet.comments.push(newComment.comment);
                  }
                });
              });
              $rootScope.newComments[tweetId] = [];
            };

          }
        }

      }
    ]);

})();