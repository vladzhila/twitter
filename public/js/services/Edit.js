(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Edit', [
      '$rootScope', '$resource', '$http', '$timeout', 
      function($rootScope, $resource, $http, $timeout) {

        function getUser() {
          $resource('/edit_page').get(function(res) {
            $rootScope.user = res.user;
          });
        }

        return {
          get: function() {
            getUser();
          },
          changeAvatar: function(url, fd) {
            $http.post(url, fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined }
            })
            .success(function(res){
              getUser();
            });
          },
          deleteAvatar: function(id) {
            $resource('/delete_avatar/' + id).delete(function(res) {
              getUser();
            });
          },
          password: function(id, data) {
            $resource('/password/' + id).save(data, function(res) {
              $timeout(function() {
                alert('Пароль успешно изменен!');
                getUser();
              }, 0);
            }, function() {
              return showErrorMsg('Текущий пароль не верный!');
            });
          },
          errorMsg: showErrorMsg
        };


        /**
         * Error massage for login and sign up
         */

        function showErrorMsg(text) {
          $rootScope.errorMessage = text;
          $timeout(function() {
            $rootScope.errorMessage = '';
          }, 3000);
        }

      }]);

})();