(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Follow', [
      '$rootScope', '$resource', 'User', 'Button', 'Tweets',
      function($rootScope, $resource, User, Button, Tweets) {

        function getUserButtonsTweets() {
          User.get();
          Button.get();
          Tweets.get();
        }

        return {
          add: function(url, param) {
            $resource(url + param).save(function(res) {
              getUserButtonsTweets();
            });
          },
          remove: function(url, param) {
            $resource(url + param).delete(function(res) {
              getUserButtonsTweets();
            });
          }
        };

      }]);

})();