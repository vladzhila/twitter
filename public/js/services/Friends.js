(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Friends', [
      '$rootScope', '$resource',
      function($rootScope, $resource) {

        function getFriend(url) {
          $resource(url).get(function(res) {
            getFriendsAndBtns(res);
          });
        }

        function getFriendsAndBtns(res) {
          $rootScope.friends = res.friends;
          $rootScope.buttons = res.buttons;
        }

        return {
          get: function() {
            getFriend('/friends_page');
          },
          queries: function() {
            getFriend('/queries_page');
          },
          apply: function(username) {
            $resource('friend/query/apply/' + username).save(function(res) {
              getFriend('/queries_page');
              getFriendsAndBtns(res);
            });
          },
          cancel: function(username) {
            $resource('friend/query/cancel/' + username).delete(function(res) {
              getFriend('/queries_page');
              getFriendsAndBtns(res);
            });
          }
        };

      }]);

})();