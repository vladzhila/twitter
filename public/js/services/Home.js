(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Home', [
      '$rootScope', '$resource',
      function($rootScope, $resource) {

        return {
          get: function() {
            $resource('/home').get(function(res) {
              $rootScope.user = res.user;
              $rootScope.tweets = res.tweets;
              $rootScope.sessionUser = res.sessionUser;
            });
          }
        };

      }]);

})();