(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Search', [
      '$rootScope', '$resource',
      function($rootScope, $resource) {

        return {
          getUsers: function(keyword) {
            $resource('/search/user').save({
              fullname : keyword,
              username : keyword
            }, function(res) {
              $rootScope.users = res.users;
            });
          }
        };

      }]);

})();