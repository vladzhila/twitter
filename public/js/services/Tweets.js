(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Tweets', [
      '$rootScope', '$resource', 'Home', 'Admin', 'socket',
      function($rootScope, $resource, Home, Admin, socket) {

        function getData() {
          switch (location.pathname) {

            case '/':
              Home.get(); break;

            case '/admin/tweets':
              Admin.tweets(); break;

            default:
              getTweets(); break;
              
          }
        }

        function getTweets() {
          $resource('/tweets' + location.pathname).get(function(res) {
            $rootScope.tweets = res.tweets;
            $rootScope.amountTweets = res.amountTweets;
          });
        }

        return {
          get: function() {
            getTweets();
          },
          add: function(userId, tweet) {

            // var tweetData = {
            //  content: tweet.content,
            //  private: tweet.private,
            //  userId: userId
            // };

            $resource('/tweet/create/' + userId).save(tweet, function(res) {
              getData();
              socket.emit('notification:tweets', res.lastTweet);
            });

            // socket.emit('add:tweet', tweetData, function(data) {
              // $rootScope.tweets = data.tweets;
              // $rootScope.amountTweets = data.tweets.length;

              // socket.emit('watch:new:tweet:frontpage', data.lastTweet);
           //  });
          },
          delete: function(userId, tweetId) {
            if (!confirm('Удалить твит?')) return;

            var dataEmit = {
              userId: userId,
              tweetId: tweetId
            };

            socket.emit('remove:tweet', dataEmit, function(data) {
              getData();
              socket.emit('notification:tweets', { tweetId: data.deletedTweetId });
            });
          },
          like: function(tweetId) {
            $resource('/tweet/like/' + tweetId).save(function() {
              getData();
            });
          },
          retweet: function(userId, tweetId) {
            $resource('/retweet/' + tweetId).save({ user: userId }, function() {
              getData();
            });
          },
          deleteRetweet: function(userId, tweetId) {
            if (!confirm('Удалить ретвит?')) return;
            $resource('/retweet/delete/' + tweetId).delete({ user: userId }, function() {
              getData();
            });
          }
        };

      }]);

})();