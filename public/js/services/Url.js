(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('Url', [
      function() {

        return {
          find: function(tweet) {
            var checkUrl = new RegExp("(^|[ \t\r\n])((ftp|http|https|gopher|mailto|news|nntp|telnet|wais|file|prospero|aim|webcal):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))"
              ,"g"
            );

            var urls = tweet.match(checkUrl);

            if (!urls) return false;

            // removing duplicates in an array urls
            urls = urls.filter(function(elem, pos) {
              return urls.indexOf(elem) == pos;
            });

            urls.forEach(function(url) {
              var link, nameUrl, urlWithoutProtocol,
                  http = 'http://',
                  https = 'https://',
                  isHttp = url.indexOf(http),
                  isHttps = url.indexOf(https);

              if (~isHttp) {
                getNameUrl(http);
              } else if (~isHttps) {
                getNameUrl(https);
              }

              nameUrl = urlWithoutProtocol || url;
              link = '<a href="' + url + '" target="_blank">' + nameUrl + '</a>';
              tweet = tweet.replace(url, link);

              function getNameUrl(protocol) {
                urlWithoutProtocol = url.replace(protocol, '');

                // delete last character "/"" in url
                if (urlWithoutProtocol.slice(-1) == '/') {
                  urlWithoutProtocol = urlWithoutProtocol.slice(0, -1);
                }

              }
            })

            return tweet;

          }
        };

      }]);

})();