(function() {
  'use strict';

  angular.module('twitterApp')
    .factory('User', [
      '$rootScope', '$resource',
      function($rootScope, $resource) {

        return {
          get: function() {
            $resource('/user' + location.pathname).get(function(res) {
              $rootScope.user = res.user;
              $rootScope.sessionUser = res.sessionUser;
            });
          },
          delete: function(id, url) {
            if (!confirm('Вы действительно ходите удалить профиль?')) return;
            $resource('/delete_profile/' + id).delete(function(res) {
              location.href = '/';
            });
          }
        };

      }]);

})();