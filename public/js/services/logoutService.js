(function() {
	'use strict';

	angular
	.module('twitterApp')
	.factory('logoutService', ['$resource',
		function($resource) {

			function logout() {
				$resource('/logout').save(function() {
					location.href = '/';
				});
			}

			return { logout: logout };
		}]);
	
})();