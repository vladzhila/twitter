(function() {
	'use strict';

	angular.module('tweetService', [])

		.factory('Tweets', ['$http', function($http) {
			return {
				get: function() {
					return $http.get('/:username/create');
				},
				create: function(tweetData) {
					// return $http.post('/create', tweetData);
				}
			}
		}]);

})();
