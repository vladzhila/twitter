(function() {
	'use strict';

	angular
	.module('twitterApp')
	.factory('validationService', [function() {

			function validator(fullname, email, username) {

				var regexFullname = /^([a-z']+(-| )?)+$/i
					, regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
					, regexUsername = /^@?(\w){1,15}$/;

				if (!regexFullname.test(fullname)) {
					return 'Не допустимый поле "Имя и фамилия"!';
				}

				if (!regexEmail.test(email)) {
					return 'Не допустимый Email адрес!';
				}

				if (!regexUsername.test(username)) {
					return 'Не допустимый "Логин!"';
				}

				return 200;
			}

			return { validator: validator };
		}]);
	
})();