(function() {
  'use strict';

  angular
    .module('twitterApp')
    .controller('signupCtrl', ['$scope', '$resource', '$timeout',
      function($scope, $resource, $timeout) {

	      $scope.signup = function() {

	      	var fullname = $scope.fullname
	      		, email    = $scope.email
	      		, username = $scope.username
	      		, password = $scope.password;


      		if (fullname && $scope.email && username && password) {

      			var regexUsername = /^@?(\w){1,15}$/
		    			, regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	    			if (!regexEmail.test(email)) {
							return showErrorMsg('Не допустимый Email адрес!');
	    			}

	    			if (!regexUsername.test(username)) {
							return showErrorMsg('Не допустимый логин!');
	    			}

	    			// REST post sign up
			      $resource('/signup').save({
		      		'fullname': fullname,
		      		'email'   : email,
				      'username': username,
				      'password': password
			      }, function() {
							location.href = '/';
			      }, function() {
							return showErrorMsg('Пользователь с данным логин или email уже есть');
			      });

      		} else {
						showErrorMsg('Заполните все поля!');
      		}

	      	function showErrorMsg(text) {
						$scope.errorMessage = text;
						$timeout(function() {
							$scope.errorMessage = '';
	      			$scope.fieldErr = false;
						}, 3000);
	      	}

	    	};

      }
  ]);

})();