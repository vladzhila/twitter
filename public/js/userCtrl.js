(function() {
	'use strict';

	angular
		.module('twitterApp')
		.controller('userCtrl', ['$scope', '$resource', '$http', 'logoutService',
			function($scope, $resource, $http, logoutService) {

				$scope.newTweet = '';
				$scope.formData = {};

				// ADD TWEET ==========================

				$scope.addTweet = function() {

					var lengthTweet = $scope.newTweet.length;

					// 1-140 symbols
					if (lengthTweet && lengthTweet <= 140) {

						$scope.formData.tweet = $scope.newTweet;

						$resource(location.pathname).save($scope.formData, function() {
							location.reload();
							$scope.formData = {}; // reset data form
						});

					}

				};

				// DELETE TWEET ==========================

				$scope.deleteTweet = function(tweet) {

					if (!confirm('Вы действительно ходите удалить твит?')) return;

					$http.post('/delete/' + tweet).then(function() {
						location.reload();
					});
				};

				$scope.logout = function() {
					logoutService.logout();	
				};

			}
		]);

})();

