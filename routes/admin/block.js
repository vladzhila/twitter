
var User = require('models/user').User
  , blockUser = require('middleware/blockUser');

// block user
// POST /admin/block/:id

exports.block = function(req, res, next) {
  User.findById(req.params.id, function(err, user) {
    if (err) return next(err);

    // block user
    user.block = !user.block;

    // if user unlocked save user in db
    if (user.block == false) { return saveUser(); }

    blockUser(req, user, next);
    saveUser();


    /**
     * Save user in db
     */
    
    function saveUser() {
      user.save(function(err) {
        if (err) return next(err);
        res.send({ user: user });
      });
    }

  });
};