var User = require('models/user').User;

exports.delete = function(req, res, next) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if (err) return next(err);
    res.send({ user: user });
  });
};
