
var User = require('models/user').User
  , Tweets = require('models/tweets').Tweets;

// load home info
// GET /admin_page

exports.get = function(req, res, next) {
  User.find(function(err, users) {
    if (err) return next(err);

    Tweets.find(function(err, tweets) {
      if (err) return next(err);

      res.send({ users: users, tweets: tweets });
    })
  });
};