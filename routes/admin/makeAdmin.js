
var User = require('models/user').User;

// make admin
// POST /admin/make_admin/:id

exports.makeAdmin = function(req, res, next) {
  var userId = req.params.id;

  if (userId == req.session.user) return;

  User.findById(userId, function(err, user) {
    if (err) return next(err);

    user.isAdmin = !user.isAdmin;

    user.save(function(err) {
      if (err) return next(err);
      res.send({ user: user });
    });
  })
};