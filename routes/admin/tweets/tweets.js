
var User = require('models/user').User
  , Tweets = require('models/tweets').Tweets;

// load tweets
// GET /admin/tweets_page

exports.get = function(req, res, next) {
  Tweets.find()
  .populate('user comments')
  .populate('comments.user')
  .exec(function(err, tweets) {
    if (err) return next(err);

    User.populate(tweets, {
      path: 'comments.user'
    }, function() {

      var arrayTweets = [];

      for (var i= 0; i< tweets.length; i++) {
        arrayTweets.push(tweets[i]);
      }

      arrayTweets.sort(function(firstDate, secondDate) {
        return new Date(secondDate.created) - new Date(firstDate.created);
      });

      res.send({
        tweets: arrayTweets,
        sessionUser: req.session.user
      });
    });
  });
};