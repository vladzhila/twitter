
var User = require('models/user').User;

// load users
// GET /admin/users_page

exports.get = function(req, res, next) {
  User.find(function(err, users) {
    if (err) return next(err);
    res.send({ users: users });
  });
};