
var Tweets = require('models/tweets').Tweets;

// attach image
// POST /attach/image/:id

exports.attach = function(req, res, next) {
  var tweetId = req.params.id;

  Tweets.findById(tweetId)
  .populate('user')
  .exec(function(err, tweet) {
    if (err) return next(err);

    tweet.image = tweetId;

    tweet.save(function(err, tweet) {
      if (err) return next(err);
      res.send({ tweet: tweet });
    });
  });

};