
var User       = require('models/user').User
  , validation = require('../validation')
  , HttpError  = require('error').HttpError
  , AuthError  = require('error/authError').AuthError;

// user sign up
// POST /signup

exports.post = function(req, res, next) {

  var fullname = req.body.fullname
    , email    = req.body.email
    , username = req.body.username
    , password = req.body.password;

  if (validation(res, fullname, email, username)) {
    User.signup(fullname, email, username, password, function(err, user) {
      if (err) {
        if (err instanceof AuthError) {
          return next(new HttpError(403, err.message));
        } else {
          return next(err);
        }
      }

      req.session.user = user._id;
      res.send();
    });
  }
};