
var Tweets = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments;

// add comment
// POST /comment/add/:id

exports.add = function(req, res, next) {
  Tweets.findById(req.params.id, function(err, tweet) {
    if (err) return next(err);

    tweet.save(function(err) {
      if (err) return next(err);

      var comment = new Comments({
        content: req.body.comment,
        user:    req.session.user,
        tweet:   req.params.id
      });

      comment.save(function(err, comment) {
        if (err) return next(err);

        Tweets.findById(comment.tweet, function(err, tweet) {
          if (err) return next(err);

          tweet.comments.push(comment._id);

          tweet.save(function(err) {
            if (err) return next(err);
          });
        });
        res.send();
      });
    });
  });
};