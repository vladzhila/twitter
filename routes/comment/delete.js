
var Tweets = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments;

// delete comment
// DELETE /comment/delete/:id

exports.delete = function(req, res, next) {

  // delete comment form collection
  Comments.findByIdAndRemove(req.params.id, function(err, comment) {
    if (err) return next(err);

    Tweets.findById(comment.tweet, function(err, tweet) {
      if (err) return next(err);

      var idx = tweet.comments.indexOf(comment._id)

      // delete ObjectID in tweet.comments
      tweet.comments.splice(idx, 1);

      tweet.save(function(err) {
        if (err) return next(err);
      });
    });
    
    res.send();
  });
};