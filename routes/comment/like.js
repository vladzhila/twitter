
var Comments = require('models/comments').Comments;

// like comment
// POST /comment/like/:id

exports.like = function(req, res, next) {
  Comments.findById(req.params.id, function(err, comment) {
    if (err) return next(err);

    var sessionId = req.session.user;
    var idx = comment.likes.indexOf(sessionId);

    // add or cancel LIKE
    (~idx) ?
      // cancel
      comment.likes.splice(idx, 1) :
      // add
      comment.likes.push(sessionId);

    comment.save(function(err) {
      if (err) return next(err);

      Comments.find(function(err, comments) {
        if (err) return next(err);
        
        res.send({ comments: comments });
      });
    });

  });
};