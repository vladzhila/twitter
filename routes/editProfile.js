
var User   = require('models/user').User
	, Tweets = require('models/tweets').Tweets

// load edit profile
// GET /edit_prodile

exports.get = function(req, res) {
	res.render('edit-profile');
};

// edit fullname, username, email
// POST /edit_info/:id

exports.edit = function(req, res, next) {
	User.findById(req.params.id, function(err, user) {
		if (err) return next(err);

		user.fullname = req.body.fullname;
		user.username = req.body.username;
		user.email = req.body.email;

		user.save(function(err) {
			if (err) return next(err);
			res.redirect('/');
		});
	});
};

// change password
// POST /password/:id

exports.password = function(req, res, next) {
	User.findById(req.params.id, function(err, user) {
		if (err) return next(err);

		if (user.checkPassword(req.body.oldPassword)) {
			user.password = req.body.newPassword;
		} else {
			return next(err);
		}

		user.save(function(err) {
			if (err) return next(err);
			res.redirect('/');
		});
	});
};

// delete profile
// POST /delete_profile/:id

exports.delete = function(req, res, next) {
	// delete all tweets
	Tweets.remove({ user: req.params.id }, function(err, tweets) {
		if (err) return next(err);
	})

	// session destroy
	req.session.destroy();

	// find user and delete him
	User.findByIdAndRemove(req.params.id, function(err, user) {
		if (err) next(err);
		res.redirect('/');
	});
};