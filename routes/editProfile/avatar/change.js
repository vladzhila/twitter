
var User = require('models/user').User
  , fs   = require('fs');

// change user avatar
// POST /avatar/:id

exports.change = function(req, res, next) {
  User.findById(req.params.id, function(err, user) {
    if (err) return next(err);

    if (user.avatar != 'avatar.png') {
      fs.unlink('public/img/' + user.avatar, function(err) {
        changeAvatar();
      });
    } else {
      changeAvatar();
    }

    function changeAvatar() {
      user.avatar = req.file.filename;
      user.save(function(err) {
        if (err) return next(err);
        res.send({ user: user });
      });
    }

  });
};