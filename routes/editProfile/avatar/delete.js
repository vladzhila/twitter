
var User = require('models/user').User
  , fs   = require('fs');

// delete user avatar
// POST /delete_avatar/:id

exports.delete = function(req, res, next) {
  User.findById(req.params.id, function(err, user) {
    if (err) return next(err);

    fs.unlink('public/img/' + user.avatar, function(err) {
      user.avatar = 'avatar.png';
      user.save(function(err) {
        if (err) return next(err);
        res.send({ user: user });
      });
    });
  });
};