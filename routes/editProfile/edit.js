
var User = require('models/user').User;

// get info edit profile
// GET /edit_page

exports.get = function(req, res) {
  User.findById(req.session.user, function(err, user) {
    if (err) return next(err);
    res.send({ user: user });
  });
};