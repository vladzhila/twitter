
var User       = require('models/user').User
  , validation = require('../validation');

// edit fullname, username, email
// POST /edit_info/:id

exports.edit = function(req, res, next) {

  var fullname = req.body.fullname
    , username = req.body.username
    , email    = req.body.email;

  if (validation(res, fullname, email, username)) {
    User.findOneAndUpdate({ _id: req.params.id }, {
      fullname: fullname,
      username: username,
      email: email
    }, { upsert: true }, function(err, user) {
      if (err) return next(err);
      res.send({ user: user });
    });
  }
};