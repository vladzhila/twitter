
var User = require('models/user').User;

// change password
// POST /password/:id

exports.password = function(req, res, next) {
  User.findById(req.params.id, function(err, user) {
    if (err) return next(err);

    if (user.checkPassword(req.body.oldPassword)) {
      user.password = req.body.newPassword;
    } else {
      return next(err);
    }

    user.save(function(err) {
      if (err) return next(err);
      res.send({ user: user });
    });
  });
};