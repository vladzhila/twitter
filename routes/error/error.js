
var HttpError = require('error').HttpError;

exports.get = function(req, res, next) {
  return next(new HttpError(404, 'Страница не найдена'));
};