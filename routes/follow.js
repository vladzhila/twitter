
var User = require('models/user').User;

// follow user
// post /follow/:id

exports.post = function(req, res, next) {
	var userId = req.session.user;
	var followerId = req.params.id;

	User.findById(userId, function(err, user) {
		if (err) return next(err);

		user.follows.unshift(followerId);

		User.findById(followerId, function(err, user) {
			if (err) return next(err);

			var index = user.followers.indexOf(userId);
			(index == -1) ? user.followers.push(userId) : next(err);

			user.save(function(err) {
				if (err) return next(err);
			});
		});

		user.save(function(err) {
			if (err) return next(err);
			res.redirect('/');
		});
	});
};