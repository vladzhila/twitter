
var User = require('models/user').User;

// follow user
// post /follow/:id

exports.follow = function(req, res, next) {
  var userId     = req.session.user;
  var followerId = req.params.id;

  User.findById(userId, function(err, user) {
    if (err) return next(err);

    var index = user.follows.indexOf(followerId);
    if (~index) return res.redirect('/');

    // follow to user
    user.follows.unshift(followerId);

    User.findById(followerId, function(err, user) {
      if (err) return next(err);

      var index = user.followers.indexOf(userId);
      (index == -1) ? user.followers.push(userId) : next(err);

      user.save(function(err) {
        if (err) return next(err);
      });
    });

    user.save(function(err) {
      if (err) return next(err);
      res.send({ user: user });
    });
  });
};