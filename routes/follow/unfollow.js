
var User = require('models/user').User;

// unfollow user
// POST /unfollow/:id

exports.unfollow = function(req, res, next) {
  var userId       = req.session.user;
  var unfollowerId = req.params.id;

  User.findById(userId, function(err, user) {
    if (err) return next(err);

    var index = user.follows.indexOf(unfollowerId);
    if (!~index) return res.redirect('/');

    // unfollow from user
    user.follows.splice(index, 1);

    User.findById(unfollowerId, function(err, user) {
      if (err) return next(err);

      var index = user.followers.indexOf(userId);
      (~index) ? user.followers.splice(index, 1) : next(err);

      user.save(function(err) {
        if (err) return next(err);
      });
    });

    user.save(function(err) {
      if (err) return next(err);
      res.send({ user: user });
    });
  });
};