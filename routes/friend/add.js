
var User = require('models/user').User;

// add to friends
// POST friend/add/:id

exports.add = function(req, res, next) {
  
  var userId   = req.session.user;
  var friendId = req.params.id;

  User.findById(friendId, function(err, user) {
    if (err) return next(err);

    var indexQuery = user.queryFriends.indexOf(userId);
    if (~indexQuery) return res.redirect('/');

    // add to follower
    var indexFollower = user.followers.indexOf(userId);
    if (!~indexFollower) {
      User.findById(userId, function(err, user) {
        if (err) return next(err);

        user.follows.unshift(friendId);

        User.findById(friendId, function(err, user) {
          if (err) return next(err);

          user.followers.push(userId);

          user.save(function(err) {
            if (err) return next(err);
          });
        });

        user.save(function(err) {
          if (err) return next(err);
        });
      });
    }

    user.queryFriends.unshift(userId);

    user.save(function(err) {
      if (err) return next(err);

      res.send({ user: user });
    });

  });
};