
var User = require('models/user').User;

// cancel query to friend
// POST friend/cancel/:id

exports.cancel = function(req, res, next) {
  var userId = req.session.user;
  var friendId = req.params.id;

  User.findById(friendId, function(err, user) {
    if (err) return next(err);

    var index = user.queryFriends.indexOf(userId);
    if (!~index) return res.redirect('/');

    user.queryFriends.splice(index, 1);

    user.save(function(err) {
      if (err) return next(err);
      res.send({ user: user });
    });

  });
};