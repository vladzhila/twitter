
var User = require('models/user').User;

// load friends
// GET /friends_page

exports.get = function(req, res, next) {

  var userId = req.session.user;

  User.findById(userId, function(err, user) {
    var friends = []
      , amountFriends = user.friends.length
      , counter = 0;

    for (var i = 0; i < amountFriends; i++) {
      var id = user.friends[i];
      User.findById(id, function(err, user) {
        var friend = {};
        friend.avatar = user.avatar;
        friend.fullname = user.fullname;
        friend.username = user.username;

        friends.push(friend);

        counter++;

        if (amountFriends == counter) {
          res.send({
            friends: friends,
            buttons: false
          });
        }
      });
    }

  });
};