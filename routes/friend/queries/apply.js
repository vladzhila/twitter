
var User = require('models/user').User;

// apply query to friend
// POST /friends/query/apply/:username

exports.apply = function(req, res, next) {

  var userId = req.session.user;

  User.findOne({ username: req.params.username }, function(err, user) {
    if (err) return next(err);

    var idxFriend = user.friends.indexOf(userId);
    if (~idxFriend) return res.redirect('/');

    user.friends.push(userId);

    var idxFollower = user.followers.indexOf(userId);
    if (!~idxFollower) {
      user.followers.push(userId);
    }

    var friendId = user._id;

    user.save(function(err) {
      if (err) return next(err);

      User.findById(userId, function(err, user) {
        if (err) return next(err);

        var index = user.queryFriends.indexOf(friendId);
        user.queryFriends.splice(index, 1);

        user.friends.push(friendId);

        var idxFollow = user.follows.indexOf(friendId);
        if (!~idxFollow) {
          user.follows.push(friendId);
        }

        user.save(function(err) {
          if (err) return next(err);
          res.send({ user: user });
        });
      });
    });

  });
};