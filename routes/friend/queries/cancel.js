
var User = require('models/user').User;

// cancel query to friend
// POST /friends/query/cancel/:username

exports.cancel = function(req, res, next) {
  User.findOne({ username: req.params.username }, function(err, user) {
    if (err) return next(err);

    var friendId = user._id;

    User.findById(req.session.user, function(err, user) {
      if (err) return next(err);

      var index = user.queryFriends.indexOf(friendId);
      if (!~index) return res.redirect('/');

      user.queryFriends.splice(index, 1);

      user.save(function(err) {
        if (err) return next(err);
        res.send({ user: user });
      });

    });
  });
};