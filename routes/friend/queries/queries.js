
var User = require('models/user').User;

// queries to friend
// GET /queries_page

exports.get = function(req, res, next) {

  var userId = req.session.user;

  User.findById(userId, function(err, user) {
    var friends = []
      , amountQueries = user.queryFriends.length
      , counter = 0;

    for (var i = 0; i < amountQueries; i++) {
      var id = user.queryFriends[i];
      User.findById(id, function(err, user) {
        var friend = {};
        friend.avatar = user.avatar;
        friend.fullname = user.fullname;
        friend.username = user.username;

        friends.push(friend);

        counter++;

        if (amountQueries == counter) {
          res.send({
            friends: friends,
            buttons: true
          });
        }

      });
    }

  });
};