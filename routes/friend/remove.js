
var User = require('models/user').User;

// remove user from friends
// POST friend/remove/:id

exports.remove = function(req, res, next) {

  var userId = req.session.user;
  var friendId = req.params.id;

  User.findById(userId, function(err, user) {
    if (err) return next(err);

    var idxFriend = user.friends.indexOf(friendId);
    if (!~idxFriend) return res.redirect('/');

    user.friends.splice(idxFriend, 1);

    User.findById(friendId, function(err, user) {
      if (err) return next(err);

      var idxUser = user.friends.indexOf(userId);
      user.friends.splice(idxUser, 1);

      user.save(function(err) {
        if (err) return next(err);
      });
    });

    user.save(function(err) {
      if (err) return next(err);
      res.send({ user: user });
    });
  });
};