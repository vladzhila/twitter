
var User = require('models/user').User
	, Tweets = require('models/tweets').Tweets
	, async = require('async');

// load home
// GET /home

exports.get = function(req, res, next) {
	if (req.session.user) {
		async.waterfall([
			function(cb) {
				User.findById(req.session.user, cb);
			},
			function(user, cb) {
				(user.follows.length) ? cb(null, user) : res.render('home');
			},
			function(user, cb) {
				var arrayTweets = []
					, amountFollowers = user.follows.length
					, counter = 0;

				for (var i = 0; i < user.follows.length; i++) {
					Tweets
					.find({ user: user.follows[i] })
					.populate('user').exec(function(err, tweets) {
						if (err) return next(err);

						for (var j = 0; j < tweets.length; j++) {
							arrayTweets.push(tweets[j]);
						}

						counter++;

						if (amountFollowers == counter) {
							arrayTweets.sort(function(firstDate, secondDate) {
								return new Date(secondDate.created) - new Date(firstDate.created);
							});
							cb(null, arrayTweets);
						}
					});

				}
			}
		], function(err, results) {
			if (err) return next(err);

			res.render('home', {
				tweets: results
			});
		});
	} else {
		res.render('login');
	}
};