
var User      = require('models/user').User
  , Tweets    = require('models/tweets').Tweets
  , async     = require('async')
  , blockUser = require('middleware/blockUser');

// load home
// GET /home

exports.get = function(req, res, next) {
  
  if (!req.session.user) return;

  async.waterfall([
    function(cb) {
      User.findById(req.session.user, cb);
    },
    function(user, cb) {

      // user is blocked
      if (user.block) blockUser(req, user, next);

      (user.follows.length) ?
        cb(null, user) : res.send({ user: user, sessionUser: req.session.user });
    },
    function(user, cb) {
      var arrayTweets = []
        , amountFollowers = user.follows.length
        , counter = 0;

      for (var i = 0; i < amountFollowers; i++) {
        // check follower in friends
        var index = user.friends.indexOf(user.follows[i]);

        (~index) ? outputTweets(true) : outputTweets(false);

        /**
         * Output tweets of following
         * @param  {[Boolean]} private [Private tweets only for friends]
         */

        function outputTweets(private) {
          Tweets
          .find({ user: user.follows[i] })
          .populate('user comments comments.user')
          .exec(function(err, tweets) {
            if (err) return next(err);

            User.populate(tweets, {
              path: 'comments.user'
            }, function() {

              for (var j = 0; j < tweets.length; j++) {
                if (private || !tweets[j].private) {
                  arrayTweets.push(tweets[j]);
                } 
              }

              counter++;

              if (amountFollowers == counter) {
                sortTweetsByDate();
              }
            });

          });
        }

        function sortTweetsByDate() {
          arrayTweets.sort(function(firstDate, secondDate) {
            return new Date(secondDate.created) - new Date(firstDate.created);
          });
          cb(null, arrayTweets);
        }

      } // for end
    }
  ], function(err, tweets) {
    if (err) return next(err);

    User.findById(req.session.user, function(err, user) {
      if (err) return next(err);

      res.send({
        user: user,
        tweets: tweets,
        sessionUser: user._id
      });

    });
  });

};