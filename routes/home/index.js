
// render home
// GET /home

exports.get = function(req, res, next) {
  (req.session.user) ?
    res.render('index') : res.render('login');
};