
var checkAuth = require('middleware/checkAuth')
  , renderAdmin = require('middleware/renderAdmin')
  , deleteUser = require('middleware/deleteUser')

  // Upload images
  , multer = require('multer')
  , upload = multer({ dest: 'public/img/' });


  // Attach image
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/img/attached/');
  },
  filename: function (req, file, cb) {
    cb(null, req.params.id);
  }
});

var attachImg = multer({ storage: storage });


module.exports = function(app) {


  /**
   * Auth
   */

    // sign up
  app.post('/signup', require('./auth/signup').post);

    // login
  app.post('/login', require('./auth/login').post);

    // logout
  app.post('/logout', require('./auth/logout').post);


  /**
   * Render partials angular route
   */

    // render partials
  app.get('/partials/:name', require('./partials/index').get);

    // render admin partials
  app.get('/partials/admin/:name', require('./partials/index').admin);


  /**
   * Load user page (/:username)
   */

    // check session or another user page
  app.get('/check-user/:username', require('./user/user').check);

    // session
  app.get('/current-user', require('./user/user').current);

    // another
  app.get('/another-user', require('./user/user').another);


  /**
   * Home
   */

    // render home page
  app.get('/', require('./home/index').get);

    // get info
  app.get('/home', require('./home/home').get);


  /**
   * Admin
   */

  var adminOptions = [checkAuth, renderAdmin];

    // render home admin
  app.get('/admin', adminOptions, require('./admin/home/index').get);

    // render users admin
  app.get('/admin/users', adminOptions, require('./admin/users/index').get);

    // render tweets admin
  app.get('/admin/tweets', adminOptions, require('./admin/tweets/index').get);

    // get admin info
  app.get('/admin_page', adminOptions, require('./admin/home/home').get);

    // get users
  app.get('/admin/users_page', adminOptions, require('./admin/users/users').get);

    // get tweets
  app.get('/admin/tweets_page', adminOptions, require('./admin/tweets/tweets').get);

    // make admin
  app.post('/admin/make_admin/:id', adminOptions, require('./admin/makeAdmin').makeAdmin);

    // block user
  app.post('/admin/block/:id', adminOptions, require('./admin/block').block);

    // delete profile
  app.delete('/delete_user/:id', deleteUser, require('./admin/deleteUser').delete);

  /**
   * Search
   */

    // search user
  app.post('/search/user', require('./search/index').search);


  /**
   * Edit
   */

    // render edit page
  app.get('/edit_profile', checkAuth, require('./editProfile/index').get);

    // get edit info
  app.get('/edit_page/', require('./editProfile/edit').get);

    // edit fullname, username, email
  app.post('/edit_info/:id', require('./editProfile/fullname-username-email').edit);

    // change avatar
  app.post('/avatar/:id', upload.single('file'), require('./editProfile/avatar/change').change);

    // delete avatar
  app.delete('/delete_avatar/:id', require('./editProfile/avatar/delete').delete);

    // change password
  app.post('/password/:id', require('./editProfile/password').password);

    // delete profile
  app.delete('/delete_profile/:id', deleteUser, require('./editProfile/deleteProfile').delete);


  /**
   * Tweet
   */

    // get
  app.get('/tweets/:username', checkAuth, require('./tweet/tweets').get);

    // create
  app.post('/tweet/create/:id', require('./tweet/create').create);

    // like
  app.post('/tweet/like/:id', require('./tweet/like').like);

    // delete
  app.delete('/tweet/delete/:id', require('./tweet/delete').delete);


  /**
   * Attach
   */

    // image
  app.post('/attach/image/:id', attachImg.single('file'), require('./attach/image').attach);


  /**
   * Retweet
   */

    // add
  app.post('/retweet/:id', require('./retweet/retweet').add);

    // delete
  app.delete('/retweet/delete/:id', require('./retweet/retweet').delete);


  /**
   * Comment
   */

    // add
  app.post('/comment/add/:id', require('./comment/add').add);

    // like
  app.post('/comment/like/:id', require('./comment/like').like);

    // delete
  app.delete('/comment/delete/:id', require('./comment/delete').delete);


  /**
   * Buttons
   */

    // get
  app.get('/buttons/:username', checkAuth, require('./user/buttons').get);


  /**
   * Follow
   */

    // follow
  app.post('/follow/:id', require('./follow/follow').follow);

    // unfollow
  app.delete('/unfollow/:id', require('./follow/unfollow').unfollow);


  /**
   * Friend
   */

    // render friends page
  app.get('/friends', checkAuth, require('./friend/index').get);

    // get friends
  app.get('/friends_page', checkAuth, require('./friend/friends').get);

    // add
  app.post('/friend/add/:id', require('./friend/add').add);

    // cancel
  app.delete('/friend/cancel/:id', require('./friend/cancel').cancel);

    // remove
  app.delete('/friend/remove/:id', require('./friend/remove').remove);


  /**
   * Friend query
   */

    // render friends page
  app.get('/friends_queries', checkAuth, require('./friend/queries/index').get);

    // get friends queries
  app.get('/queries_page', checkAuth, require('./friend/queries/queries').get);

    // apply
  app.post('/friend/query/apply/:username', require('./friend/queries/apply').apply);

    // remove
  app.delete('/friend/query/cancel/:username', require('./friend/queries/cancel').cancel);


  /**
   * User
   */

    // render user page
  app.get('/:username', checkAuth, require('./user/index').get);

    // get info
  app.get('/user/:username', require('./user/user').get);


  /**
   * Error
   */

  app.get('*', checkAuth, require('./error/error').get);

};