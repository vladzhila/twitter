
var User      = require('models/user').User
	, HttpError = require('error').HttpError
	, AuthError = require('error/authError').AuthError;

// user login
// POST /login

exports.post = function(req, res, next) {
	
	var username = req.body.username
		, password = req.body.password;

	User.authorize(username, password, function(err, user) {
		if (err) {
			return (err instanceof AuthError) ? 
				next(new HttpError(403, err.message)) : next(err);
		}

		req.session.user = user._id;
		res.send({});
	});

};