
// Render partials angular route
// GET /partials/:name

exports.get = function(req, res) {
  res.render('partials/' + req.params.name);
};

// Render partials angular route for admin pages
// GET /partials/admin/:name

exports.admin = function(req, res) {
  res.render('partials/admin/' + req.params.name);
};