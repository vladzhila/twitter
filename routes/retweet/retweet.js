
var User   = require('models/user').User
  , Tweets = require('models/tweets').Tweets;

// retweet
// POST /tweet/retweet/:id

exports.add = function(req, res, next) {
  User.findById(req.session.user, function(err, user) {
    if (err) return next(err);

    var io = req.app.get('io'),
        tweetId = req.params.id,
        userId = req.body.user;

    var idx = user.retweets.indexOf(tweetId);
    if (~idx) return;

    user.retweets.push(tweetId);

    Tweets.findById(tweetId, function(err, tweet) {
      if (err) return next(err);
      tweet.retweets.push(userId);

      saveTweet(tweet);
    });

    saveUser(user, res);
    io.emit('get:user:tweets');
  });
};


// delete retweet
// DELETE /tweet/retweet/delete/:id

exports.delete = function(req, res, next) {
  User.findById(req.session.user, function(err, user) {
    if (err) return next(err);

    var tweetId = req.params.id,
        userId  = req.body.user,
        idxUser = user.retweets.indexOf(tweetId);

    user.retweets.splice(idxUser, 1);

    Tweets.findById(tweetId, function(err, tweet) {
      if (err) return next(err);

      var idxTweet = tweet.retweets.indexOf(userId);
      tweet.retweets.splice(idxTweet, 1);

      saveTweet(tweet);
    });

    saveUser(user, res);
    socket(req);
  });
};


/**
 * Common
 */

function saveTweet(tweet) {
  tweet.save(function(err) {
    if (err) return next(err);
  });
}

function saveUser(user, res) {
  user.save(function(err) {
    if (err) return next(err);
    res.send({ user: user });
  });
}