
var User = require('models/user').User;

// search users
// POST /search/user

exports.search = function(req, res, next) {

  var regexFullname = { fullname: {$regex: req.body.fullname} };
  var regexUsername = { username: {$regex: req.body.username} };

  User.find({ $or: [regexFullname, regexUsername] }, function(err, users) {
    if (err) return next(err);
    res.send({ users: users });
  });

};