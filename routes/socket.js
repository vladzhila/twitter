
module.exports = function(req) {

  var io = req.app.get('io')
    , sid = req.session.id
    , connectedSockets = io.of('/').in('user:room:' + req.user.username).connected;
  
  Object.keys(connectedSockets).forEach(function(socketId) {
    var socket = connectedSockets[socketId];

    // update tweets info on front page
    socket.emit('tweets:frontpage');
    // update tweets info in current and another user
    socket.emit('get:user:tweets');
  });

};