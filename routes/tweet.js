
var User = require('models/user').User
	, Tweets = require('models/tweets').Tweets;

// create tweet
// POST /:username

exports.create = function(req, res, next) {
	User.findOne({ username: req.params.username }, function(err, user) {
		if (err) return next(err);

		// save user in db
		user.save(function(err) {
			if (err) return next(err);

			// create tweet
			var tweet = new Tweets({
				content: req.body.tweet,
				user: user._id
			});

			// save tweet in db
			tweet.save(function(err) {
				if (err) return next(err);

				// find all tweets, populate, and render to template
				Tweets.find({ user: user._id }).populate('user').exec(function(err, tweets) {
					if (err) return next(err);
					res.render('user', { tweets: tweets });
				});
			}); // tweet save
		}); // user save
	}); // find one user
};

// delete tweet
// POST /delete/:id

exports.delete = function(req, res, next) {
	Tweets.findByIdAndRemove(req.params.id, function(err, tweet) {
		if (err) return next(err);
		res.redirect('/');
	});
};