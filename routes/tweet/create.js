
var Tweets = require('models/tweets').Tweets;

// create tweet
// POST /tweet/create/:id

exports.create = function(req, res, next) {

  var userId = req.session.user;

  var tweet = new Tweets({
    content: req.body.content,
    private: req.body.private,
    user: userId
  });

  tweet.save(function(err, tweet) {
    if (err) return next(err);
    res.send({ lastTweet: tweet });
  });

};