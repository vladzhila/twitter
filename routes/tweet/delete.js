
var User     = require('models/user').User
  , Tweets   = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments
  , fs   = require('fs');

// delete tweet
// DELETE /tweet/delete/:id

exports.delete = function(req, res, next) {

  var urlTweetId = req.params.id;
  
  // delete retweet in all users
  User.find({ retweets: {$in: [urlTweetId]} }, function(err, users) {
    if (err) return next(err);

    for (var i = 0; i < users.length; i++) {
      var user = users[i];
      var idx = user.retweets.indexOf(urlTweetId);

      user.retweets.splice(idx, 1);

      user.save(function(err) {
        if (err) return next(err);
      });
    }

  });

  // delete all tweet comments
  Comments.remove({ tweet: urlTweetId }, function(err, comments) {
    if (err) return next(err);
  });

  // delete tweet
  Tweets.findByIdAndRemove(urlTweetId, function(err, tweet) {
    if (err) return next(err);

    // delete attached image
    if (tweet.image) {
      fs.unlinkSync('public/img/attached/' + tweet.image);
    }

    res.send();
  });

};