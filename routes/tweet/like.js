
var Tweets = require('models/tweets').Tweets;

// like tweet
// POST /tweet/like/:id

exports.like = function(req, res, next) {
  Tweets.findById(req.params.id, function(err, tweet) {
    if (err) return next(err);

    var sessionId = req.session.user;
    var idx = tweet.likes.indexOf(sessionId);

    // add or cancel LIKE
    (~idx) ?
      // cancel
      tweet.likes.splice(idx, 1) :
      // add
      tweet.likes.push(sessionId);

    tweet.save(function(err) {
      if (err) return next(err);

      Tweets.find(function(err, tweets) {
        if (err) return next(err);
        
        res.send({ tweets: tweets });
      });
    });

  });
};