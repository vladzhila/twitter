
var User   = require('models/user').User
  , Tweets = require('models/tweets').Tweets;

// get tweets
// GET tweets/:username

exports.get = function(req, res, next) {
  User.findOne({ username: req.params.username }, function(err, user) {
    if (err) return next(err);

    var sessionId    = req.session.user
      , userId       = user._id
      , userRetweets = user.retweets;

    // find tweet and retweet
    var criteria = {$or: [{user: userId}, {_id: {$in: userRetweets}}] };

    // current user is session user
    if (userId == sessionId) return sendTweets(criteria, userId);
    // session user in friends another use
    if (~user.friends.indexOf(sessionId)) return sendTweets(criteria, userId);


    User.findById(sessionId, function(err, user) {
      if (err) return next(err);
      if (user.isAdmin) return sendTweets(criteria, userId);

      // session user isn't admin
      criteria.private = false
      return sendTweets(criteria, userId, true);
    });

    function sendTweets(criteria, userId, privateLengthTweets) {
      Tweets
      .find(criteria)
      .populate('user comments comments.user')
      .exec(function(err, tweets) {
        if (err) return next(err);

        User.populate(tweets, {
          path: 'comments.user'
        }, function() {

          if (privateLengthTweets) {
            response(tweets.length);
          } else {
            Tweets.find({ user: userId }, function(err, ownTweets) {
              if (err) return next(err);
              response(ownTweets.length);
            });
          }


          function response(length) {
            // sort tweets by date
            tweets.sort(function(firstDate, secondDate) {
              return new Date(secondDate.created) - new Date(firstDate.created);
            });

            res.send({
              tweets: tweets,
              amountTweets: length
            });
          }

        });

      });
    }

  });
};