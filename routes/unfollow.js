
var User = require('models/user').User;

// unfollow user
// POST /unfollow/:id

exports.delete = function(req, res, next) {
	var userId = req.session.user;
	var unfollowerId = req.params.id;

	User.findById(userId, function(err, user) {
		if (err) return next(err);

		var index = user.follows.indexOf(unfollowerId);
		user.follows.splice(index, 1);

		User.findById(unfollowerId, function(err, user) {
			if (err) return next(err);

			var index = user.followers.indexOf(userId);
			(index > -1) ? user.followers.splice(index, 1) : next(err);

			user.save(function(err) {
				if (err) return next(err);
			});
		});

		user.save(function(err) {
			if (err) return next(err);
			res.redirect('/');
		});
	});
};