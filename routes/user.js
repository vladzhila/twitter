
var User      = require('models/user').User
	, Tweets    = require('models/tweets').Tweets
	, HttpError = require('error').HttpError;

// load user
// GET /:username

exports.get = function(req, res, next) {

	User.findOne({ username: req.params.username }, function(err, user) {
		if (err) return next(err); 
		if (!user) return next(new HttpError(404, 'Пользователь не найден'));

		var currentUserId = req.session.user + '',
				foundUserId   = user._id + '';

		if (currentUserId == foundUserId) {
			findRenderTweets('user');
		} else {
			User.findById(req.session.user, function(err, user) {
				if (err) return next(err);

				var index = user.follows.indexOf(foundUserId);

				(index > -1) ? 
					findRenderTweets('another-user', true) :
					findRenderTweets('another-user', false);

			});
		}

		/**
		 * Find all user tweets and render template
		 * @param  { String } template [template that render]
		 */
		function findRenderTweets(template, toggleBtnFollow) {
			Tweets.find({ user: user._id }, function(err, tweets) {
				if (err) return next(err);

				if (template == 'user') {
					res.render(template, { tweets: tweets });
				} else {
					res.render(template, {
						user: user,
						tweets: tweets,
						toggle: toggleBtnFollow
					});
				}
			});
		}
	});
};