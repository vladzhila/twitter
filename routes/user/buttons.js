
var User   = require('models/user').User
  , Tweets = require('models/tweets').Tweets;

// get buttons
// GET buttons/:username

exports.get = function(req, res, next) {

  // find user by username url
  User.findOne({ username: req.params.username }, function(err, user) {
    if (err) return next(err); 

    var sessionUser = req.session.user
      , anotherUser = user._id;

    if (sessionUser == anotherUser) return;

    User.findById(anotherUser, function(err, user) {
      if (err) return next(err);

      // check session user id in queries of friends another user
      var idxQueryFriend = user.queryFriends.indexOf(sessionUser),
          // check session user id in friends another user
          idxFriend = user.friends.indexOf(sessionUser),
          // options buttons that need show
          options = {};

      // session user in queries of friends another user
      if (~idxQueryFriend) {
        options.reader = options.cancelFriend = true;
        return sendButtons(options);
      }

      // session user in friends another user
      if (~idxFriend) {
        options.reader = options.removeFriend = true;
        return sendButtons(options);
      }

      User.findById(sessionUser, function(err, user) {
        if (err) return next(err);

        // check another user in queries to friends of session user
        var idxQueryFriend = user.queryFriends.indexOf(anotherUser);

        // check another user in followers of session user
        var idxFollow = user.follows.indexOf(anotherUser);

        if (~idxQueryFriend) {
          if (~idxFollow) {
            options.unfollow = options.buttons = true;
            return sendButtons(options);
          }
          options.follow = options.buttons = true;
          return sendButtons(options);
        }

        // another user in follow of session user
        if (~idxFollow) {
          options.unfollow = options.friend = true;
          return sendButtons(options);
        } else {
          options.follow = options.friend = true;
          return sendButtons(options);
        }
      });
      
      /**
       * Options to display buttons
       * @param  { Object } options [buttons that need to show]
       */

      function sendButtons(options) {
        res.send({
          buttons:            options.buttons      || false,
          toggleFollow:       options.follow       || false,
          toggleUnfollow:     options.unfollow     || false,
          toggleReader:       options.reader       || false,
          toggleFriend:       options.friend       || false,
          toggleCancelFriend: options.cancelFriend || false,
          toggleRemoveFriend: options.removeFriend || false
        });
      }

    });
  });
};