
var User      = require('models/user').User
  , HttpError = require('error').HttpError;

// render user page
// GET /:username

exports.get = function(req, res, next) {
  User.findOne({ username: req.params.username }, function(err, user) {
    if (err) return next(err); 
    if (!user) return next(new HttpError(404, 'Страница не найдена'));

    res.render('index');
  });
};