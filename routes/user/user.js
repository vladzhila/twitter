
var User = require('models/user').User;

// load user
// GET /:username

exports.get = function(req, res, next) {
  User.findOne({ username: req.params.username }, function(err, user) {
    if (err) return next(err); 

    res.send({
      user: user,
      sessionUser: req.session.user
    });
    
  });
};

// check current or another user page
// GET /check-user/:username

exports.check = function(req, res, next) {
  User.findById(req.session.user, function(err, user) {
    if (err) return next(err); 

    (req.params.username == user.username) ?
      res.render('check-user', { current: true }) :
      res.render('check-user', { current: false });
  });
};

// current user
// GET /check-user

exports.current = function(req, res, next) {
  res.render('current-user');
};

// another user
// GET /another-user

exports.another = function(req, res, next) {
  User.findById(req.session.user, function(err, user) {
    if (err) return next(err); 
    res.render('another-user', { isAdmin: user.isAdmin });
  });
};