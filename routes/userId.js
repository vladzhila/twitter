
var express = require('express')
  , router = express.Router()
  , User = require('models/user').User
  , HttpError = require('error').HttpError
  , ObjectID = require('mongodb').ObjectID;

router.get('/user/:id', function(req, res, next) {

  try {
    var id = new ObjectID(req.params.id);
  } catch (e) {
    return next(new HttpError(404, 'Не найдено'));
  }

  User.findById(id, function(err, user) {
    if (err) return next(err);

    if (!user) {
      return next(new HttpError(404, 'Пользователь не найден'));
    }
    res.json(user);
  });
});

module.exports = router;