
var User = require('models/user').User;

// show all users
// GET /users

exports.get = function(req, res, next) {
	User.find(function(err, users) {
		var usernames = [];

		users.forEach(function(user) {
			usernames.push(user.username);
		});

		res.render('users', {
			usernames: JSON.stringify(usernames)
		});
	});
};