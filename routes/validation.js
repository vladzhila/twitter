
module.exports = function(res, fullname, email, username) {

  var regexFullname = /^([a-zA-Zа-яА-Я']+(-| )?)+$/i
    , regexEmail    = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    , regexUsername = /^[A-Za-z0-9_]{1,15}$/;

  if (!regexFullname.test(fullname))
    return sendRegexError('Не допустимый поле "Имя и фамилия"');

  if (!regexEmail.test(email))
    return sendRegexError('Не допустимый Email адрес');

  if (!regexUsername.test(username))
    return sendRegexError('Не допустимый "Логин"');

  return true;

  function sendRegexError(err) {
    res.status(400).send({ error: err });
  }

};