
var User = require('models/user').User
  , HttpError = require('error').HttpError;

module.exports = function(io, socket) {
  socket.on('block:user', function(data, cb) {

    var sid = socket.handshake.session.id;

    User.findById(sid, function(err, user) {
      if (err) throw err;

      if (user.isAdmin) {
        User.findById(data.blockUserId, function(err, user) {
          if (err) throw err;

          user.block = !user.block;

          user.save(function(err) {
            if (err) throw err;
            res.send({ user: user });
          });
        });
      } else {
        return next(new HttpError(403, 'Нету доступа!'));
      }
    });
  });
};