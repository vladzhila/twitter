
var Tweets = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments;

exports.add = function(data, cb) {
  var tweetId = data.tweetId;
  var senderUserId = data.senderUserId;

  var comment = new Comments({
    content: data.comment,
    user:    senderUserId,
    tweet:   tweetId
  });

  comment.save(function(err, comment) {
    if (err) throw err;

    // add comment to tweets in db
    Tweets.findById(comment.tweet, function(err, tweet) {
      if (err) throw err;

      tweet.comments.push(comment._id);
      tweet.save(function(err) {
        if (err) throw err;
      });
    });

    Comments.findById(comment._id)
    .populate('user')
    .exec(function(err, comment) {
      if (err) throw err;

      cb({
        tweetId: tweetId,
        comment: comment,
        senderUserId: senderUserId
      });
    });

  });

};