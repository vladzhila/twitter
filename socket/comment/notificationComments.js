
module.exports = function(io, socket) {
  socket.on('notification:comments', function(data) {
    var connectedSockets = io.sockets.connected;

    // for each connected user
    Object.keys(connectedSockets).forEach(function(socketId) {
      var socket = connectedSockets[socketId];

      // send only another users
      var anotherUser = socket.handshake.user._id != data.senderUserId;

      if (!data.deletedComment) {
        // add comment to notification
        if (anotherUser)
          socket.emit('add:comment:notification', data);
      } else {
        // remove comment from notification
        if (anotherUser)
          socket.emit('remove:comment:notification', { deletedComment: data.deletedComment });
      }

    });
  });   
};