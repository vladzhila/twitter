
var Tweets = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments;

exports.remove = function(data, cb) {

  // delete comment form collection
  Comments.findByIdAndRemove(data.commentId, function(err, comment) {
    if (err) throw err;

    Tweets.findById(comment.tweet, function(err, tweet) {
      if (err) throw err;

      var idx = tweet.comments.indexOf(comment._id)

      // delete ObjectID in tweet.comments
      tweet.comments.splice(idx, 1);

      tweet.save(function(err) {
        if (err) throw err;
      });
    });

    cb({
      deletedComment: comment,
      senderUserId: data.senderUserId
    });
    
  });
};