
var config       = require('config')
  , async        = require('async')
  , cookieParser = require('cookie-parser')
  , sessionStore = require('libs/sessionStore')
  , HttpError    = require('error').HttpError
  , User         = require('models/user').User;


function loadSession(sid, callback) {

  // sessionStore callback is not quite async-style!
  sessionStore.load(sid, function(err, session) {
    if (arguments.length == 0) {
      // no arguments => no session
      return callback(null, null);
    } else {
      return callback(null, session);
    }
  });

}

function loadUser(session, callback) {

  if (!session.user) return callback(null, null);

  User.findById(session.user, function(err, user) {
    if (err) return callback(err);
    if (!user) return callback(null, null);

    callback(null, user);
  });

}



module.exports = function(server) {

  var secret = config.get('session:secret');
  var sessionKey = config.get('session:key');

  var io = require('socket.io').listen(server);

  // configuration
  io.set('origins', 'localhost:*');


  /**
   * Socket authorization
   */

  io.use(function (socket, next) {
    var handshakeData = socket.request;

    async.waterfall([
      function (callback) {
        // get sid
        var parser = cookieParser(secret);
        parser(handshakeData, {}, function (err) {
          if (err) return callback(err);

          var sid = handshakeData.signedCookies[sessionKey];

          loadSession(sid, callback);
        });
      },
      function (session, callback) {
        if (!session) {
          return callback(new HttpError(401, "No session"));
        }

        socket.handshake.session = session;
        loadUser(session, callback);
      },
      function (user, callback) {
        if (!user) {
          return callback(new HttpError(403, "Anonymous session may not connect"));
        }
        callback(null, user);
      }
    ], function (err, user) {

      if (err) {
        if (err instanceof HttpError) {
          return next(new Error('not authorized'));
        }
        next(err);
      }

      socket.handshake.user = user;
      next();

    });

  });



  /**
   * Socket conncection
   */

  io.on('connection', function(socket) {

    var userRoom = "user:room:" + socket.handshake.user.username;
    socket.join(userRoom);


    /**
     * Tweet
     */

      // add
    // socket.on('add:tweet', require('./tweet/add').add);
    
      // remove
    socket.on('remove:tweet', require('./tweet/remove').remove);

      // show notification new tweets on frontpage
    require('./tweet/notificationTweets')(io, socket);


    /**
     * Comments
     */

      // add
    socket.on('add:comment', require('./comment/add').add);

      // remove
    socket.on('remove:comment', require('./comment/remove').remove);

      // notification new comments on anothers pages
    require('./comment/notificationComments')(io, socket);


    /**
     * Admin
     */

      // block user
    require('./admin/block')(io, socket);
    
  });

  return io;
};