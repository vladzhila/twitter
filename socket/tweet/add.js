
var Tweets = require('models/tweets').Tweets;

exports.add = function(data, cb) {
  var userId = data.userId;

  var tweet = new Tweets({
    content: data.content,
    private: data.private,
    user: userId
  });

  tweet.save(function(err, tweet) {
    if (err) throw err;

    Tweets.find({ user: userId })
    .populate('user')
    .exec(function(err, tweets) {
      if (err) throw err;

      // sort tweets by date
      tweets.sort(function(firstDate, secondDate) {
        return new Date(secondDate.created) - new Date(firstDate.created);
      });

      cb({ lastTweet: tweet, tweets: tweets });

    });
  });

};