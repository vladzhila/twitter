
var User = require('models/user').User
  , Tweets = require('models/tweets').Tweets;

module.exports = function(io, socket) {
  socket.on('notification:tweets', function(tweet) {
    var connectedSockets = io.sockets.connected
      , tweetForFriends = tweet.private; // true or false

    User.findById(socket.handshake.session.user, function(err, user) {
      if (err) throw err;

      // for each connected user
      Object.keys(connectedSockets).forEach(function(socketId) {
        var socket = connectedSockets[socketId];

        if (!tweet.tweetId) {
          // add tweet to notification
          var userId = socket.handshake.user._id;

          if (tweetForFriends) {
            if (~user.friends.indexOf(userId)) { emitMsgNewTweet(); }
          } else {
            if (~user.followers.indexOf(userId)) { emitMsgNewTweet(); }
          }

          function emitMsgNewTweet() {
            Tweets.findById(tweet._id)
            .populate('user')
            .exec(function(err, tweet) {
              if (err) throw err;
              socket.emit('add:tweet:notification', { tweet: tweet });
            });
          }

        } else {
          // remove tweet from notification
          socket.emit('remove:tweet:notification', { tweetId: tweet.tweetId });
        }

      });
    });
  });   
};
