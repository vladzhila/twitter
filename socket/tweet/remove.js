
var User     = require('models/user').User
  , Tweets   = require('models/tweets').Tweets
  , Comments = require('models/comments').Comments
  , fs   = require('fs');


exports.remove = function(data, cb) {
  var tweetId = data.tweetId;
  
  // delete retweet in all users
  User.find({ retweets: {$in: [tweetId]} }, function(err, users) {
    if (err) throw err;

    for (var i = 0; i < users.length; i++) {
      var user = users[i];
      var idx = user.retweets.indexOf(tweetId);

      user.retweets.splice(idx, 1);

      user.save(function(err) {
        if (err) throw err;
      });
    }
  });

  // delete all tweet comments
  Comments.remove({ tweet: tweetId }, function(err, comments) {
    if (err) throw err;
  });

  // delete tweet
  Tweets.findByIdAndRemove(tweetId, function(err, tweet) {
    if (err) throw err;

    // delete attached image
    if (tweet.image) {
      fs.unlinkSync('public/img/attached/' + tweet.image);
    }
  });

  cb({ deletedTweetId: tweetId });

  // Tweets.find({ user: data.userId })
  // .populate('user')
  // .exec(function(err, tweets) {
  //   if (err) console.log( err );

  //   // sort tweets by date
  //   tweets.sort(function(firstDate, secondDate) {
  //     return new Date(secondDate.created) - new Date(firstDate.created);
  //   });

  //   cb({ deletedTweetId: tweetId, tweets: tweets });
  // });
    
};